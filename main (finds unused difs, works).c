#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/mman.h>


#include <ctype.h>
int stricmp (const char *p1, const char *p2, size_t len)
{
	register unsigned char *s1 = (unsigned char *) p1;
	register unsigned char *s2 = (unsigned char *) p2;
	unsigned char c1, c2;
	
	do
	{
		c1 = (unsigned char) toupper((int)*s1++);
		c2 = (unsigned char) toupper((int)*s2++);
		if (c1 == '\0')
		{
			return c1 - c2;
		}
	}
	while (c1 == c2 && len--);
	
	if (!len) return -1;
	
	return c1 - c2;
}



struct mis_file {
	char *path;
	char *fname;
	
	char *img_path;
	
	int num_interiors;
	char **interior_paths;
	
	int num_found;
	
	char *author;
	char *name;
	char *mode;
};

struct cla_sort {
	struct mis_file **miss;
	char **images;
	char **difs;
	
	char *difs_found;
	
	int num_difs, num_miss, num_imgs;
	int cur_difs, cur_miss, cur_imgs;
	const char *path;
};


struct folder_data {
	char *path;
	
	struct folder_data *parent;
	struct folder_data **folders;
	
	struct file_data {
		char *name;
		char *ext;
		unsigned int bytes;
		char flag;
	} *files;
	
	int num_files;
	int num_folders;
	int level;
	
	unsigned long long total_bytes;
};

struct queue {
	struct node {
		char *item;
		struct node *next;
	} *head, *tail;

	int size;
};

void alloc_image_data (char ***dst, int y, int x)
{
	*dst = malloc(sizeof(char *) * y);
	
	while (y--)
		(*dst)[y] = calloc(sizeof(char), x);
}

void destroy_image_data (char **src, int y)
{
	while (y--)
		free(src[y]);
	
	free(src); 
}


int search_2d (char **src, int num_entries, const char *str)
{
	while (num_entries)
		if (!stricmp(src[--num_entries], str, 64))
			return num_entries;
	
	return -1;
}

int get_extension (const char *src_path, char *dst, int max)
{
	int tmp, len, ret;
	
	tmp = len = (int)strlen(src_path);
	
	while (--tmp >= 0 && src_path[tmp] != '\0' && src_path[tmp] != '.')
		if (src_path[tmp] == '/' || src_path[tmp] == '\0')
			return -1;
	
	if (tmp <= 0)
		return -1;
	
	if (((ret = (len - tmp)) <= 0) || ret > max)
		return -1;
	
	memcpy(dst, src_path + tmp, ret);
	
	return ret;
	
}


int count_instances (int offset, off_t size, char *file_address, const char *search, int len) 
{
	int i, k, count = 0; 
	
	for (i = offset; i < size - len; i++) {
		if (!memcmp(file_address + i, search, len)) {
			for (k = i; k >= 0 && file_address[k] != '"' && file_address[k]; k--)
				;
			
			if (k > 0)
				count++;
		}
	}
	
	return count;
}



void push (struct queue *queue, const char *src) 
{
	struct node *n = calloc(sizeof(struct node), 1);
	
	n->item = strdup(src);
	n->next = NULL;
	
	if (queue->head == NULL)
		queue->head = n;
	else
		queue->tail->next = n;
	
	queue->tail = n;
	queue->size++;
}
char *pop (struct queue *queue) 
{
	struct node *head = queue->head;
	char *item = NULL;
	
	if (queue->size <= 0)
		return item;
	
	item = head->item;
	
	queue->head = head->next;
	queue->size--;
	
	free(head);
	
	return item;
}


char *get_tag (char *data, int len, const char *tag)
{
	char *ret = NULL;
	char *buf = NULL;
	int i;
	int tlen = (int)strlen(tag);
	
	if (!(ret = strstr(data, tag)))
		return NULL;
	
	for (i = (int)(ret - data) + tlen; i < len; i++)
		if (data[i] == '"')
			break;
	
	if (i == len)
		return NULL;
	
	buf = calloc(1, i - (ret - data) + tlen);
	strncpy(buf, ret + tlen, i - (int)(ret - data) - tlen);
	
	return buf;
}

const char *get_last (const char *dir, unsigned char cmp)
{
	const char *start = dir;
	
	while (*dir) dir++;
	
	while (dir >= start && *dir != cmp) dir--;
	
	return (dir == start) ? NULL : dir;
}

/* returns: 
 -1 	error, missing one or more '.' or '/'
 0	incorrect length, different filenames
 1	same filename */
int cmp_file_name (const char *dir1, const char *dir2)
{
	const char *d1_start = get_last(dir1, '/');
	const char *d2_start = get_last(dir2, '/');
	
	if (!d1_start || !*d1_start) d1_start = dir1;
	if (!d2_start || !*d2_start) d2_start = dir2;
	
	int d1_end = (int)(get_last(dir1, '.') - d1_start) + 1;
	int d2_end = (int)(get_last(dir2, '.') - d2_start) + 1;
	
	if (!d1_start || !d2_start || d1_end <= 0 || d2_end <= 0) return -1;
	
	if (d1_end != d2_end) return 0;
	
	return !strncmp(d1_start, d2_start, d1_end);
}

/* assumes that path is known to be a .mis */
int check_mis (const char *path, struct cla_sort *data)
{
	struct stat st;
	char *file_address;
	char **difs;
	int fd, i, k, x, instances, dpos = 0;
	
	if ((fd = open(path, O_RDONLY)) < 0)
		return -1;
	
	if (fstat(fd, &st) != 0 || !S_ISREG(st.st_mode) || S_ISDIR(st.st_mode)) 
		return -1;
	
	if ((file_address = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED) 
		return -1;
	
	instances = count_instances(0, st.st_size, file_address, ".dif", 4);
	
	printf("%d difs\n", instances);
	
	if (instances <= 0)
		return instances;
	
	
	alloc_image_data(&data->miss[data->cur_miss]->interior_paths, instances, 512);
	
	difs = data->miss[data->cur_miss]->interior_paths;
	
	for (i = 0; i < st.st_size - 4; i++) {
		if (!memcmp(file_address + i, ".dif", 4)) {
			k = i;
			
			while (k >= 0 && strncmp(file_address + k, "/data/", 6) && file_address[k] != '"')
				k--;
			if (k <= 0 || file_address[k] == ';') {
				puts("couldnt extract name of dif\n");
			} else {
				if ((i - k + 4) > 512) {
					puts("too big");
				} else {
					for (x = 0; x < dpos; x++)  /* checks for dupes */
						if (!strncmp(difs[x], file_address + k, i - k))
							break;
					
					if (x == dpos) {
						strncpy(difs[dpos++], (file_address + k), i - k + 4);
						//printf("****\t%s\n", difs[dpos - 1]);
					}
			}
			}
		}
	}
	
	int t = dpos;
	
	while (t < instances) {
		free(difs[t]);
		difs[t] = NULL;
		t++;
	}
	
	data->miss[data->cur_miss]->author = get_tag(file_address, 
						     (int)st.st_size, "artist = \"");
	data->miss[data->cur_miss]->name = get_tag(file_address, 
						   (int)st.st_size, "name = \"");
	data->miss[data->cur_miss]->mode = get_tag(file_address, 
						   (int)st.st_size, "type = \"");
	
	close(fd);
	
	if (munmap(file_address, st.st_size)) {
		perror("munmap");
		return -1;
	}
	
	return dpos;
}


/* copies the file "src_path" into the directory "dst_path" */
ssize_t file_copy (const char *dst_path, const char *src_path)
{
	int src_fd, dst_fd;
	ssize_t res;
	char wpath[1024];
	const char *filename;
	struct stat st;
	
	if (stat(src_path, &st) < 0 || S_ISDIR(st.st_mode)) return -1;
	
	if (stat(dst_path, &st) < 0 || !S_ISDIR(st.st_mode)) return -1;
	
	strncpy(wpath, dst_path, sizeof(wpath) - 1);
	if ((filename = get_last(src_path, '/')) == NULL || !*filename) return -1;

	strncat(wpath, filename, sizeof(wpath) - 1);
	
	fprintf(stderr, "[%s]: copying: \n\t\"%s\" to: \n\t\"%s\"\n", 
		__func__, src_path, wpath);
	
	if ((src_fd = open(src_path, O_RDONLY, 0777)) < 0) return -1;
	
	if ((dst_fd = open(wpath, O_WRONLY|O_EXCL|O_CREAT, 0777)) < 0) return -1;
	
	while ((res = read(src_fd, wpath, sizeof(wpath))) > 0)
		if (write(dst_fd, wpath, res) != res) return -1;
	
	close(src_fd);
	close(dst_fd);
	
	return 0;
}

int count_filetype_in_dir (const char *path, const char *extension)
{
	int count = 0;
	char ext[64];
	struct dirent *ds;
	struct stat st;
	char wpath[1024];
	DIR *dr;
	
	if ((dr = opendir(path)) == NULL)
		return 0;
	
	while ((ds = readdir(dr))) {
		if (*(ds->d_name) != '.') {
			memset(wpath, 0, sizeof(wpath));
			snprintf(wpath, sizeof(wpath), "%s/%s", path, ds->d_name);

			if (stat(wpath, &st) >= 0) {
				
				if (S_ISDIR(st.st_mode)) {
					count += count_filetype_in_dir(wpath, extension);
				} else {
					memset(ext, 0, sizeof(ext));
					if (get_extension(wpath, ext, sizeof(ext)) < 0)
						printf("cannot get ext\n");
					else
						if (!strncmp(ext, extension, sizeof(ext)))
							count++;
				}
			}
		}
	}
	
	closedir(dr);
	
	return count;

	
}

int get_interiors (const char *src_path, struct cla_sort *data)
{
	struct dirent *ds;
	struct stat st;
	char wpath[1024], ext[64];
	DIR *dr;
	
	if ((dr = opendir(src_path)) == NULL)
		return -1;
	

	while ((ds = readdir(dr))) {
		if (*(ds->d_name) == '.')
			continue;
		memset(&st, 0, sizeof(struct stat));
		memset(wpath, 0, sizeof(wpath));
		snprintf(wpath, sizeof(wpath), "%s/%s", src_path, ds->d_name);
		
		if (stat(wpath, &st) < 0)
			continue;
			
		if (!S_ISDIR(st.st_mode)) {
			memset(ext, 0, sizeof(ext));
			if (get_extension(wpath, ext, sizeof(ext)) < 0)
				printf("cant get ext\n");
			else if (!strncmp(ext, ".dif", 4)) {
				if ((data->cur_difs) > data->num_difs) {
					fprintf(stderr, "error: unexpected dif\n");
				} else {
					char *data_ptr = wpath;
					if ((data_ptr = strstr(wpath, "/data/")) == NULL) {
						printf("invalid path\n");
						data_ptr = wpath;
					}
					
					data->difs[data->cur_difs++] = strdup(data_ptr);
					fprintf(stderr, "dif #%d: %s\n", data->cur_difs, data_ptr);
				}
			}
		} else {
			if (get_interiors(wpath, data) < 0) {
				fprintf(stderr, "get interiors failed %s\n", wpath);
			}
		}
	}
	
	closedir(dr);
	
	return 0;
	
}


int process_missions (const char *src_path, struct cla_sort *data)
{
	struct dirent *ds;
	struct stat st;
	char wpath[1024], ext[64];
	DIR *dr;
	int i, j;
	struct queue missing_interiors;
	struct queue broken_mis;

	if ((dr = opendir(src_path)) == NULL)
		return -1;
	
	memset(&missing_interiors, 0, sizeof(struct queue));
	memset(&broken_mis, 0, sizeof(struct queue));
	int is_broken;
	
	while ((ds = readdir(dr))) {
		if (*(ds->d_name) == '.')
			continue;
			
		memset(wpath, 0, sizeof(wpath));
		snprintf(wpath, sizeof(wpath), "%s/%s", src_path, ds->d_name);
		is_broken = 0;
		
		int offset = 0; 
		if (stat(wpath, &st) < 0)
			continue;
		
		if (S_ISDIR(st.st_mode)) {
			process_missions(wpath, data);
			continue;
		}
			
		memset(ext, 0, sizeof(ext));
		
		if (get_extension(wpath, ext, sizeof(ext)) < 0)
			continue;
		else if (strncmp(ext, ".mis", 4))
			continue;
		if ((data->cur_miss) > data->num_miss)
			continue;
	
		data->miss[data->cur_miss] = calloc(sizeof(struct mis_file), 1);

		offset = check_mis(wpath, data);					
		data->miss[data->cur_miss]->num_interiors += offset;
		
		fprintf(stderr, "found mis %s, contains %d interiors\n", ds->d_name, offset);
		
		for (i = 0; i < offset; i++) {
			int index = 0;
			const char *tmpptr = data->miss[data->cur_miss]->interior_paths[i];
			printf("searching for %s (%d)\n", tmpptr, data->cur_difs);
			if ((index = search_2d(data->difs, data->cur_difs, tmpptr)) < 0) {
				printf("cannot find interior %s\n", tmpptr);
				
				is_broken = 1;
				
				if (missing_interiors.size > 0) {
					struct node *head = missing_interiors.head;
					
					printf("%d item(s):\n", missing_interiors.size);
					for (j = 0; j < missing_interiors.size; j++) {
						if (!strncmp(head->item, tmpptr, 512))
							break;
					
						head = head->next;
					}
					
					if (j == missing_interiors.size) {
						printf("found new missing interior %s\n", tmpptr);
						push(&missing_interiors, tmpptr);
					}
				} else {
					push(&missing_interiors, tmpptr);
				}
				
				
			} else {
				data->difs_found[index] = 1;
				printf("found interior %s\n", data->miss[data->cur_miss]->interior_paths[i]);
				free(data->miss[data->cur_miss]->interior_paths[i]);
				data->miss[data->cur_miss]->interior_paths[i] = NULL;
			}
			
		}
		
		
		if (is_broken) {
			is_broken = 0;
			push(&broken_mis, ds->d_name);
		}
		
		free(data->miss[data->cur_miss]->interior_paths);
		free(data->miss[data->cur_miss]->name);
		free(data->miss[data->cur_miss]->author);
		free(data->miss[data->cur_miss]->mode);
		free(data->miss[data->cur_miss]);
		
		data->miss[data->cur_miss]->interior_paths = NULL;
		data->miss[data->cur_miss]->name = NULL;
		data->miss[data->cur_miss]->author = NULL;
		data->miss[data->cur_miss]->mode = NULL;
		data->miss[data->cur_miss] = NULL;
		
		data->cur_miss++;
	}
	
	char *tmp;
	
	closedir(dr);
	
	while (missing_interiors.size) {
		tmp = pop(&missing_interiors);
		
		printf("missing interior %s\n", tmp);
		
		free(tmp);
		tmp = NULL;
	}
	
	while (broken_mis.size > 0) {
		tmp = pop(&broken_mis);
		
		printf("broken mis %s\n", tmp);
		
		char outbuf[1024];
		memset(outbuf, 0, sizeof(outbuf));
		
		snprintf(outbuf, sizeof(outbuf), "%s/%s", src_path, tmp);
		
		//file_copy("/Users/nobody1/Desktop/broken", outbuf);
		//remove(outbuf);
		
		free(tmp);
		tmp = NULL;
	}
	
		
	return 0;
	
}

int main (int argc, char **argv)
{
	struct cla_sort sort;
	const char *path = "/Users/nobody1/.ssh/cla/data";
	
	sort.num_difs = count_filetype_in_dir(path, ".dif");
	sort.num_miss = count_filetype_in_dir(path, ".mis");
	sort.num_imgs = count_filetype_in_dir(path, ".png") + 
			count_filetype_in_dir(path, ".jpg") + 
			count_filetype_in_dir(path, ".bmp") +
			count_filetype_in_dir(path, ".jpeg");
	
	sort.cur_difs = sort.cur_imgs = sort.cur_miss = 0;
	
	fprintf(stderr, "directory \"%s\" contains %d .dif, %d .mis, and %d images\n",
		path, sort.num_difs, sort.num_miss, sort.num_imgs);
	
	
	sort.difs_found = calloc(sort.num_difs, sizeof(char));
	
	sort.difs = calloc(sort.num_difs, sizeof(char *));
	sort.miss = calloc(sort.num_miss, sizeof(struct mis_file *));
	sort.images = calloc(sort.num_imgs, sizeof(char *));
	
	sort.path = path;
	
	if (get_interiors(path, &sort) < 0)
		printf("error\n");
	
	if (process_missions(path, &sort) < 0)
		printf("error\n");
	
	/* NEW CODE, now that we have all of the data, split it */
	
	/* file hierarchy: ~/SP/author/working/interiors */
	
	if (sort.num_difs != sort.cur_difs) {
		fprintf(stderr, "error: found %d difs, expected %d\n", sort.cur_difs, sort.num_difs);
	} else if (sort.num_imgs != sort.cur_imgs) {
		fprintf(stderr, "error: found %d imgs, expected %d\n", sort.cur_imgs, sort.num_imgs);
	} else if (sort.num_miss != sort.cur_miss) {
		fprintf(stderr, "error: found %d miss, expected %d\n", sort.cur_miss, sort.num_miss);
	} else {

	}
	
	int j = 0;
	
	for (j = 0; j < sort.num_difs; j++) {
		if (!sort.difs_found[j]) {
			char wpath[1024];
			memset(wpath, 0, sizeof(wpath));
			
			snprintf(wpath, sizeof(wpath), "/Users/nobody1/.ssh/cla%s", sort.difs[j]);
			
			fprintf(stderr, "interior %s not used\n", sort.difs[j]);
			
			//if (file_copy("/Users/nobody1/Desktop/unused_difs", wpath) < 0) {
			//	printf("copy error\n");
			//} else {
			//	remove(wpath);
			//}
			
		}
	}
	
	while (sort.num_difs--)
		free(sort.difs[sort.num_difs]);
	while (sort.num_miss--) {
		if (sort.miss[sort.num_miss]) {
			printf("name: %s, author: %s, mode: %s\n", 
			       sort.miss[sort.num_miss]->name, 
			       sort.miss[sort.num_miss]->author, 
			       sort.miss[sort.num_miss]->mode);
			
			free(sort.miss[sort.num_miss]->name);
			free(sort.miss[sort.num_miss]->author);
			free(sort.miss[sort.num_miss]->mode);
			
		int i = sort.miss[sort.num_miss]->num_interiors;
		while (i--) {
			printf("%d: %s\n", i, sort.miss[sort.num_miss]->interior_paths[i]);
			free(sort.miss[sort.num_miss]->interior_paths[i]);
		}
		free(sort.miss[sort.num_miss]->interior_paths);
		free(sort.miss[sort.num_miss]);
		}
	}
	while (sort.num_imgs--)
		free(sort.images[sort.num_imgs]);
	
	free(sort.difs);
	free(sort.miss);
	free(sort.images);
	
	return 0;
}