
unsigned int print_integer (unsigned char *dst, unsigned int val, unsigned int base)
{
	unsigned int digits = 0;
#ifdef X86_64_SUPPORTED
	asm volatile ("xorq %%rsi, %%rsi\n"
		      "movl %%ebx, %%eax\n"
		"pi_count_digits:\n"
		      "xorl %%edx, %%edx\n"
		      "cmpl $0, %%eax\n"
		      "je pi_endcd\n"
		      "incq %%rsi\n"
		      "divl %%ecx\n"		/* eax = quotient, edx = remainder */
		      "jmp pi_count_digits\n"
		"pi_endcd:\n"
		      "cmpq $0, %%rsi\n"
		      "je pi_end\n"
		      "addq %%rsi, %%rdi\n"
		      "movl %%ebx, %%eax\n"
		"pi_loop2:\n"
		      "xorl %%edx, %%edx\n"
		      "divl %%ecx\n"
		      "decq %%rdi\n"
		      "addb $48, %%dl\n"	/* '0' = 0x30 = 48 */
		      "movb %%dl, (%%rdi)\n"
		      "cmpl $0, %%eax\n"
		      "jnz pi_loop2\n"
		"pi_end:\n"
		      : "=S" (digits)
		      : "D" (dst), "b" (val), "c" (base)
		      : "rdx");
#else
	unsigned int tmp;

	for (tmp = val; tmp; tmp /= base)
		digits++;

	if (!digits)
		return 0;
	
	for (dst += digits; val; val /= base)
		*--dst = (val % base) + '0';
#endif
	return digits;
}

char *print_tag (char *name_ptr, char *end, const char *tag)
{
#ifdef X86_64_SUPPORTED
	asm volatile (	"movq %%rdx, %%rcx\n"
			"subq %%rdi, %%rcx\n"
			"cld\n"
		"pt_loop1:\n"
			"decq %%rcx\n"
			"jz pt_end\n"
			"lodsb\n"
			"stosb\n"
			"testb %%al, %%al\n"
			"jnz pt_loop1\n"
		"pt_end:\n"
			: "=D" (name_ptr)
			: "D" (name_ptr), "d" (end), "S" (tag)
			: "al");
#else
	while (name_ptr < end && *tag)
		*name_ptr++ = *tag++;
#endif
	return name_ptr;
}

int print_entry (char *name_ptr, const char *tag, char *path, int level, 
			int max, int bytes, char *ext)
{
	char *end = name_ptr + max;
	char *start = name_ptr;
	
	while (level-- && name_ptr < end)
		*name_ptr++ = '\t';
		
	name_ptr = print_tag(name_ptr, end, tag);
	name_ptr = print_tag(name_ptr, end, path);

	name_ptr = print_tag(name_ptr, end, " (");
	name_ptr += print_integer((unsigned char *)name_ptr, bytes, 10);
	name_ptr = print_tag(name_ptr, end, " bytes)");
	
	name_ptr = print_tag(name_ptr, end, " (ext ");
	name_ptr = print_tag(name_ptr, end, ext);
	name_ptr = print_tag(name_ptr, end, ")\n");

	*name_ptr++ = '\0';
	
	return (int)(name_ptr - start);
}

void print_file_tree (struct folder_data *src)
{
	int j, length;
	char name_buffer[1024];

	if ((length = print_entry(&name_buffer[0], "folder ", src->path, src->level, 
				  sizeof(name_buffer) - 1, (unsigned int)src->total_bytes, 
				  "folder")) < 0)
		return;
	
	write(STDOUT_FILENO, name_buffer, length);

	for (j = 0; j < src->num_files; j++) {
		memset(name_buffer, 0, sizeof(name_buffer) - 1);
		
		if ((length = print_entry(&name_buffer[0], "file ", src->files[j].name, 
					  src->level + 1, sizeof(name_buffer) - 1, 
					  src->files[j].bytes, src->files[j].ext)) < 0)
			return;
			
		write(STDOUT_FILENO, name_buffer, length);
	}
	
	for (j = 0; j < src->num_folders; j++)
		print_file_tree(src->folders[j]);
}

struct file_data *find_largest_file (struct folder_data *src, struct file_data *max_ptr, 
			unsigned long long *max_bytes)
{
	int i;

	for (i = 0; i < src->num_files; i++) {
		if (src->files[i].bytes > *max_bytes) {
			*max_bytes = src->files[i].bytes;
			max_ptr = &src->files[i];
		}
	}
		  
	for (i = 0; i < src->num_folders; i++) 
		max_ptr = find_largest_file(src->folders[i], max_ptr, max_bytes);
	
	return max_ptr;
}


int print_entry (char *name_ptr, const char *tag, char *path, int level, 
			int max, int bytes, char *ext)
{
	char *end = name_ptr + max;
	char *start = name_ptr;
	
	while (level-- && name_ptr < end)
		*name_ptr++ = '\t';
		
	snprintf(name_ptr, (int)(end - name_ptr), "%s %s (%d bytes) (ext %s)\n",
		tag, path, bytes, ext);
		
	return (int)strlen(start);
}

void print_file_tree (struct folder_data *src)
{
	int j, length;
	char name_buffer[1024];

	if ((length = print_entry(&name_buffer[0], "folder ", src->path, src->level, 
				  sizeof(name_buffer) - 1, (unsigned int)src->total_bytes, 
				  "folder")) < 0)
		return;
	
	write(STDOUT_FILENO, name_buffer, length);

	for (j = 0; j < src->num_files; j++) {
		memset(name_buffer, 0, sizeof(name_buffer) - 1);
		
		if ((length = print_entry(&name_buffer[0], "file ", src->files[j].name, 
					  src->level + 1, sizeof(name_buffer) - 1, 
					  src->files[j].bytes, src->files[j].ext)) < 0)
			return;
			
		write(STDOUT_FILENO, name_buffer, length);
	}
	
	for (j = 0; j < src->num_folders; j++)
		print_file_tree(src->folders[j]);
}

/*
	struct file_data filedt, *max;
	unsigned long long max_bytes = 0;
	
	max = find_largest_file(fdata, &filedt, &max_bytes);
	
	printf("largest file: %s (%d bytes)\n", max->name, max->bytes);
	
*/
