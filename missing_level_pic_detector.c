#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>

struct folder_data {
	char *path;
	
	struct folder_data *parent;
	struct folder_data **folders;
	
	struct file_data {
		char *name;
		char *ext;
		unsigned int bytes;
		char flag;
	} *files;
	
	int num_files;
	int num_folders;
	int level;
	
	unsigned long long total_bytes;
};

struct queue {
	struct node {
		char *item;
		struct node *next;
	} *head, *tail;

	int size;
};

void push (struct queue *queue, const char *src) 
{
	struct node *n = calloc(sizeof(struct node), 1);
	
	n->item = strdup(src);
	n->next = NULL;
	
	if (queue->head == NULL)
		queue->head = n;
	else
		queue->tail->next = n;
	
	queue->tail = n;
	queue->size++;
}
char *pop (struct queue *queue) 
{
	struct node *head = queue->head;
	char *item = NULL;
	
	if (queue->size <= 0)
		return item;
	
	item = head->item;
	
	queue->head = head->next;
	queue->size--;
	
	free(head);
	
	return item;
}

const char *get_last (const char *dir, unsigned char cmp)
{
	const char *start = dir;
	
	while (*dir) dir++;
	
	while (dir >= start && *dir != cmp) dir--;
	
	return (dir == start) ? NULL : dir;
}

/* copies the file "src_path" into the directory "dst_path" */
ssize_t file_copy (const char *dst_path, const char *src_path)
{
	int src_fd, dst_fd;
	ssize_t res;
	char wpath[1024];
	const char *filename;
	struct stat st;
	
	if (stat(src_path, &st) < 0 || S_ISDIR(st.st_mode)) return -1;
	
	if (stat(dst_path, &st) < 0 || !S_ISDIR(st.st_mode)) return -1;
	
	strncpy(wpath, dst_path, sizeof(wpath) - 1);
	if ((filename = get_last(src_path, '/')) == NULL || !*filename) return -1;

	strncat(wpath, filename, sizeof(wpath) - 1);
	
	fprintf(stderr, "[%s]: copying: \n\t\"%s\" to: \n\t\"%s\"\n", 
		__func__, src_path, wpath);
	
	if ((src_fd = open(src_path, O_RDONLY, 0777)) < 0) return -1;
	
	if ((dst_fd = open(wpath, O_WRONLY|O_EXCL|O_CREAT, 0777)) < 0) return -1;
	
	while ((res = read(src_fd, wpath, sizeof(wpath))) > 0)
		if (write(dst_fd, wpath, res) != res) return -1;
	
	close(src_fd);
	close(dst_fd);
	
	return 0;
}

/* copied from c standard lib */
int stricmp (const char *p1, const char *p2, size_t len)
{
	register unsigned char *s1 = (unsigned char *) p1;
	register unsigned char *s2 = (unsigned char *) p2;
	unsigned char c1, c2;
	
	do {
		c1 = (unsigned char) toupper((int)*s1++);
		c2 = (unsigned char) toupper((int)*s2++);
		if (c1 == '\0')
			return c1 - c2;
	} while (c1 == c2 && len--);
	
	if (!len) return -1;
	
	return c1 - c2;
}

/* returns: 
	-1 	error, missing one or more '.' or '/'
	0	incorrect length, different filenames
	1	same filename */
int cmp_file_name (const char *dir1, const char *dir2)
{
	const char *d1_start = get_last(dir1, '/');
	const char *d2_start = get_last(dir2, '/');
	
	if (!d1_start || !*d1_start) d1_start = dir1;
	if (!d2_start || !*d2_start) d2_start = dir2;
	
	int d1_end = (int)(get_last(dir1, '.') - d1_start) + 1;
	int d2_end = (int)(get_last(dir2, '.') - d2_start) + 1;

	if (!d1_start || !d2_start || d1_end <= 0 || d2_end <= 0) return -1;
		
	if (d1_end != d2_end) return 0;
		
	return !strncmp(d1_start, d2_start, d1_end);
}

int strnicmp_array (const char *src, char **array, int num, int bytes)
{
	while (--num >= 0)
		if (!stricmp(src, array[num], bytes))
			return num;

	return -1;
}

int count_dir (const char *src_path, int *num_folders, int *num_files)
{
	struct dirent *ds;
	struct stat st;
	char wpath[1024];
	DIR *dr;
	
	if (!num_folders || !num_files || (dr = opendir(src_path)) == NULL)
		return -1;
	
	(*num_files) = 0;
	(*num_folders) = 0;
	
	while ((ds = readdir(dr))) {
		if (*(ds->d_name) == '.')	continue;
			
		memset(wpath, 0, sizeof(wpath));
		snprintf(wpath, sizeof(wpath), "%s/%s", src_path, ds->d_name);
		
		if (stat(wpath, &st) < 0)	continue;

		if (!S_ISDIR(st.st_mode))	(*num_files)++;				
		else				(*num_folders)++;
	}
	
	closedir(dr);
	
	return 0;
}

void check_folder (struct folder_data *dst, struct queue *mis_queue, 
			struct queue *img_queue)
{
	char wpath[1024];
	int i, j;
	char *valid_exts[6] = {"png", "jpg", "gif", "bmp", "bm8", "jpeg"};
	
	for (i = 0; i < dst->num_files; i++) {
		if (dst->files[i].flag) continue;
			
		for (j = 0; j < dst->num_files; j++) {
			if (dst->files[j].flag || i == j || 
				cmp_file_name(dst->files[i].name, dst->files[j].name) < 1)
				continue;
				
			if (strncmp(dst->files[i].ext, ".mis", 4))
				continue;
				
			if (strnicmp_array(dst->files[j].ext + 1, valid_exts, 6, 5) < 0)
				continue;
				
			dst->files[i].flag = 1;
			dst->files[j].flag = 1;
						
			//printf("** %s %s\n", dst->files[i].name, dst->files[j].name);
			break;	
		}
		
		if (dst->files[i].flag) continue;
			
		memset(wpath, 0, sizeof(wpath));
		snprintf(wpath, sizeof(wpath), "%s/%s", dst->path, dst->files[i].name);	
		
		if (!strncmp(dst->files[i].ext, ".mis", 4)) {
			//printf("%s unpaired\n", wpath);
			dst->files[i].flag = 1;
			
			push(mis_queue, wpath);
		} else if (strnicmp_array(dst->files[i].ext + 1, valid_exts, 6, 5) >= 0) { 
			//printf("%s unpaired\n", wpath);
			dst->files[i].flag = 1;
				
			push(img_queue, wpath);
		}
	}

	for (i = 0; i < dst->num_folders; i++)
		check_folder(dst->folders[i], mis_queue, img_queue);
}

int alloc_file_tree (const char *src_path, struct folder_data **dstp, int level, 
			struct folder_data *parent, struct queue *mis_queue, 
			struct queue *img_queue)
{
	struct dirent *ds;
	struct stat st;
	char *tmps, wpath[1024];
	DIR *dr;
	struct folder_data *dst, *data_ptr;
	int cur_fileno = 0, cur_folderno = 0;
 
	dst = *dstp = calloc(sizeof(struct folder_data), 1);
	
	if (count_dir(src_path, &dst->num_folders, &dst->num_files) < 0)
		return -1;
	
	if ((dr = opendir(src_path)) == NULL)
		return -1;
	
	dst->path = strdup(src_path);
	dst->level = level;
	dst->files = calloc(sizeof(struct file_data), dst->num_files);
	dst->folders = calloc(sizeof(struct folder_data *), dst->num_folders);
	dst->parent = parent;
	
	while ((ds = readdir(dr))) {
		if (*(ds->d_name) == '.') continue;

		memset(wpath, 0, sizeof(wpath));
		snprintf(wpath, sizeof(wpath), "%s/%s", src_path, ds->d_name);
		
		if (stat(wpath, &st) < 0) continue;
			
		if (S_ISDIR(st.st_mode)) {
			alloc_file_tree(wpath, &dst->folders[cur_folderno++], 
					level + 1, dst, mis_queue, img_queue);
		} else {
			dst->files[cur_fileno].name = strdup(ds->d_name);

			if ((tmps = (char *)get_last(ds->d_name, '.')))
				dst->files[cur_fileno].ext = strdup(tmps);
			else
				dst->files[cur_fileno].ext = strdup("none");
				
			dst->total_bytes += st.st_size;
			dst->files[cur_fileno++].bytes = (unsigned int)st.st_size;
		}
	}
	
	closedir(dr);	

	for (data_ptr = dst->parent; data_ptr; data_ptr = data_ptr->parent)
		data_ptr->total_bytes += dst->total_bytes;
	
	return 0;
}

int free_file_tree (struct folder_data *src)
{
	if (!src) return -1;
	
	while (src->num_folders--)
		free_file_tree(src->folders[src->num_folders]);
	free(src->folders);
	
	while (src->num_files--) {
		free(src->files[src->num_files].name);
		free(src->files[src->num_files].ext);
	}
	
	free(src->files);
	free(src->path);
	free(src);
	
	return 0;
}

int main (int argc, char **argv)
{
	struct folder_data *fdata;
	struct queue mis_queue, img_queue;
	int i, j, mppos = 0, impos = 0;
	char *tmp, **mis_array, **img_array;
	
	memset(&mis_queue, 0, sizeof(struct queue));
	memset(&img_queue, 0, sizeof(struct queue));
	
	alloc_file_tree("/Users/nobody1/Desktop/directory_test", &fdata, 0, NULL, 
				&mis_queue, &img_queue);
				
	check_folder(fdata, &mis_queue, &img_queue);

	free_file_tree(fdata);
	
	mis_array = calloc(mis_queue.size, sizeof(char *));
	img_array = calloc(img_queue.size, sizeof(char *));
	
	while (mis_queue.size) mis_array[mppos++] = pop(&mis_queue);
	while (img_queue.size) img_array[impos++] = pop(&img_queue);
	
	for (i = 0; i < mppos; i++) {
		for (j = 0; j < impos; j++) {
			if (cmp_file_name(mis_array[i], img_array[j]) < 1)
				continue;
			
			printf("** %s\n** %s\n\n", mis_array[i], img_array[j]);
				
			if (!(tmp = (char *)get_last(mis_array[i], '/')) || !*tmp)
				continue;

			*tmp = 0;

			//if (file_copy(mis_array[i], img_array[j]) < 0)
			//	printf("error copying\n");
			//else
			//	remove(img_array[j]);
				
			free(mis_array[i]);
			mis_array[i] = NULL;
				
			break;
		}
	}
	
	for (i = 0; i < impos; i++)
		free(img_array[i]);

	for (i = 0; i < mppos; i++) {
		if (mis_array[i]) {
			printf("NO IMG FOR %s\n", mis_array[i]);
			free(mis_array[i]);
		}
	}
	
	free(mis_array);
	free(img_array);

	return 0;
}
